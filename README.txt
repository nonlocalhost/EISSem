This directory contains the D-Algorithm-implementation for the EIS-Seminar SS2017.


Downloading
-----------

  git clone git@gitlab.com:cbonse/EISSem.git

Running
-------

  python DAlgo.py -p <Path to netlist-file>

e.g.
  cd EISSem
  python DAlgo.py -p netlists/small_example.cir

  Output will be written to <Path to netlist-file>.test

Dependencies
------------

The implementation is written for Python version 3.x.
The version used was a pre-packaged version called "Anaconda"
since all necessary  packages were included and it made
managing different Python-environments much easier.

This Version can be downloaded from

    https://www.continuum.io/downloads

Bugs
----
Currently the backtracking of the D-Algorithm just moves up one step
which can lead to premature abortion of the Algorithm. One solution would be
to not just make a single hard copy of the /gates/-, /frontiers/-, and
/lines/-lists but to append them to lists themselves.

e.g.:

def initialize_network(self):
    ...
    self.gate_history = []
    self.d_front_history = []
    ...

save_history(self):
    self.gate_history.append(self.gates)
    self.d_front_history.append(self.d_front)
    ...
backtrack(self):
    self.gates = self.gate_history.pop()
    self.d_front = self.d_front_history.pop()
    ...

Also the implementation of XOR-Gates is incomplete since there was no time to
fix this in the end. The Problem here is the Definition of XOR-Gates for more
than two inputs and their respective D-cubes.
