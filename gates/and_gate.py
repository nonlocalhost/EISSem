import itertools as it

from gates.base_gate import Gate

class AND(Gate):

    def non_dominating(self):
        return '1'

    def set_fault(self, fault):
        r"""Initial fault-generation,
        Sets only one value for SA1."""
        self.consistent = True
        for key in self.outputs:
            self.outputs[key] = 'D'
        if fault == 'SA0':
            for key in self.inputs:
                self.inputs[key] = '1'
        elif fault == 'SA1':
            for key in self.inputs:
                self.inputs[key] = '0'
                break

        return self.inputs, self.outputs

    def add_schemes(self):
        r"""All possible propagation D-cubes"""
        n = len(self.inputs)
        self.schemes = {}
        self.schemes['D'] = [seq for seq in it.product(['D','1'], repeat=n)][:-1]
        self.schemes['D-'] = [seq for seq in it.product(['D-','1'], repeat=n)][:-1]
        self.schemes['0'] = [seq for seq in it.product(['0','D-'], repeat=n)][:-1]
        self.schemes['0'].extend([seq for seq in it.product(['0','D'], repeat=n)][:-1])
        self.schemes['0'].extend([seq for seq in it.product(['0','1'], repeat=n)][:-1])
        self.schemes['0'].extend([seq for seq in it.product(['0','X'], repeat=n)][:-1])
        self.schemes['1'] = [tuple(seq for seq in it.repeat('1', n))]
        self.schemes['X'] = [tuple(seq for seq in it.repeat('X', n))]
